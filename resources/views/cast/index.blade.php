@extends('layout.master')
@section('judul_1')
    Master
@endsection
@section('judul_2')
    List Cast
@endsection

@section('menu2')
menu-open
@endsection
@section('li_m0')
active
@endsection
@section('li_m1')
active
@endsection

@section('content')
<div class="col-md-12 text-right">
    <a href="/cast/create" class="btn btn-primary my-3">Tambah</a>
</div>

<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a> 
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a> 
                <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Delete</a> 
            </td>
        </tr>
          <!-- Modal -->
        <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus data?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <h2>Apakah anda yakin menghapus Cast {{$item->nama}}?</h2>
                </div>
                <div class="modal-footer">
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-primary" value="Ya">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        @empty
            <h1>Belum ada Data...</h1>
        @endforelse
    </tbody>
  </table>
  @endsection