@extends('layout.master')
@section('judul_1')
    Master
@endsection
@section('judul_2')
    Edit Cast
@endsection

@section('menu2')
menu-open
@endsection
@section('li_m0')
active
@endsection
@section('li_m1')
active
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" value="{{$cast->nama}}" class="form-control" name="iNama">
    </div>
    @error('iNama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" value="{{$cast->umur}}" class="form-control" name="iUmur">
    </div>
    @error('iUmur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea class="form-control" name="iBio" rows="3">{{$cast->bio}}</textarea>
    </div>
    @error('iBio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/cast"class="btn btn-secondary">Kembali</a>
    </div>
  </form>
@endsection