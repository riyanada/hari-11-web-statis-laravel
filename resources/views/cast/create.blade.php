@extends('layout.master')
@section('judul_1')
    Master
@endsection
@section('judul_2')
    Tambah Cast
@endsection

@section('menu2')
menu-open
@endsection
@section('li_m0')
active
@endsection
@section('li_m1')
active
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="iNama">
    </div>
    @error('iNama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="iUmur">
    </div>
    @error('iUmur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Bio</label>
        <textarea class="form-control" name="iBio" rows="3"></textarea>
    </div>
    @error('iBio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/cast"class="btn btn-secondary">Kembali</a>
    </div>
  </form>
@endsection