@extends('layout.master')
@section('judul_1')
    SanberBook
@endsection
@section('judul_2')
    Sosial Media Developer
@endsection

@section('li_b1')
active
@endsection

@section('content')
<p>Belajar Join di Media Online</p>
<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing Knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara Bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection