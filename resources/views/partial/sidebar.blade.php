<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="https://member.jabarcodingcamp.id/images/logos/LogoJCC-mobile.svg" alt="JCC" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">JCC Batch 2</span>
    </a>
    <!-- Sidebar -->
    <div iv class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://member.jabarcodingcamp.id/images/profil/profil_user_1565_image.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Riyan</a>
        </div>
      </div>
      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link @yield('li_b1')">
              <i class="nav-icon fas fa-th"></i>
              <p> Dasboard
              </p>
            </a>
          </li>
          <li class="nav-item @yield('menu')">
            <a href="#" class="nav-link @yield('li_a1')">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p> Tables <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/table" class="nav-link @yield('li_a2')">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Table</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/data-tables" class="nav-link @yield('li_a3')">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Tables</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item @yield('menu2')">
            <a href="#" class="nav-link @yield('li_m0')">
              <i class="nav-icon fas fa-database"></i>
              <p> Master <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/cast" class="nav-link @yield('li_m1')">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cast</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>