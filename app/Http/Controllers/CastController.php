<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'iNama' => 'required',
                'iUmur' => 'required',
                'iBio' => 'required'
            ],
            [
                'iNama.required' => 'Tolong masukan nama anda',
                'iUmur.required'  => 'Tolong masukan umur anda',
                'iBio.required'  => 'Tolong masukan bio anda'
            ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['iNama'], 
                'umur' => $request['iUmur'], 
                'bio' => $request['iBio']
            ]
        );

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
            [
                'iNama' => 'required',
                'iUmur' => 'required',
                'iBio' => 'required'
            ],
            [
                'iNama.required' => 'Tolong masukan nama anda',
                'iUmur.required'  => 'Tolong masukan umur anda',
                'iBio.required'  => 'Tolong masukan bio anda'
            ]
        );

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(['nama' => $request['iNama'], 'umur' => $request['iUmur'], 'bio' => $request['iBio']]);
        return redirect('cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('cast');
    }
}
